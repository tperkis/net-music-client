/*
  barebones node client for Matt Ingalls' net-music-server (https://gitlab.com/sonomatics/net-music-server)
    tim perkis 1/2019

   as of today 1/25/2019 doesn't support any server features but login, logout, write and callback on data received.

   USAGE:   
    mu = require(<path to net-music-client.js>)
    mu.connect(8124, '127.0.0.1', ()=>{console.log('connected!')})
    mu.onMsg(callback) // set up callback function action on received messages...
    mu.login('tim')
    mu.send('target', 'msg fields etc etc ')  – or args as an array: mu.send('target', [1,2.0, 'some_string'])

    that's it!

 */

 var net = require('net')
 var skt = new net.Socket()

 exports.connect = function(port, ip, callback){
    exports.port = port
    exports.ip   = ip
    skt.connect(port,ip, callback)
 }

 // msg to send can be array of strings and/or nums or just a string itself
 exports.send = (to, args)=>{
  if(args.constructor === Array) { args = args.join(' ')}
  skt.write(to + ' ' + args + ';')
 }

 exports.login = (name)=>
  skt.write('login '+name+';')

 exports.logout = (name)=>{
  skt.write('logout '+name+';')
 }

 exports.onMsg = (callback)=>{
  var buf;
  skt.on('data', (data) =>{
    var str = data.toString()
    callback(str.slice(0,-1))
  })
 }

//=================  if __main __  TESTS =======================

if (require.main === module) {

   var testPrint = function(str){ console.log(str)}

   console.log( "netMusClient TESTS")

   exports.connect(8124, '127.0.0.1', ()=>(console.log("CONNECTED")))

   exports.onMsg(testPrint)

   exports.login("tim")

   exports.send("all", ['an array to all', 1.0, 2.0,3.0])
   exports.send("tim", "a string to tim")


  // these tests act weird..  testPrint() only runs once, with all raw messages (including ';' run together... )
  // but in normal usage, it seems exports.onMsg() works perfectly... I don't get it.


} // TESTS
